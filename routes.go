package main

import (
	"log"

	"github.com/gorilla/mux"
)

// AddAppRoutes will add the routes for the application.
func AddAppRoutes(route *mux.Router) {
	log.Println("Loadeding Routes...")

	route.HandleFunc("/signin", SignInUser).Methods("POST")
	route.HandleFunc("/signup", SignUpUser).Methods("POST")
	route.HandleFunc("/user", GetUserDetails).Methods("GET")

	log.Println("Routes are Loaded.")
}